# Getting Started

### Reference Documentation
For further reference, please consider the following sections:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.4.0/maven-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/2.4.0/maven-plugin/reference/html/#build-image)
* [Spring Web](https://docs.spring.io/spring-boot/docs/2.4.0/reference/htmlsingle/#boot-features-developing-web-applications)

### Guides
The following guides illustrate how to use some features concretely:

* [Building a RESTful Web Service](https://spring.io/guides/gs/rest-service/)
* [Serving Web Content with Spring MVC](https://spring.io/guides/gs/serving-web-content/)
* [Building REST services with Spring](https://spring.io/guides/tutorials/bookmarks/)
* [Accessing data with MySQL](https://spring.io/guides/gs/accessing-data-mysql/)

# Ogólny przebieg projektu

1. Zrozumienie tematu
2. Model danych
    - papierowy i ustalenie relacji @OneToOne, @ManyToOne, @OneToMany, @ManyToMany
    - właścicielstwo relacji (mappedBy)
    - zaczytywanie danych po relacji (eager vs lazy loading)
3. Stworzenie klas typu Repository
4. Dane testowe
    - zdefiniowanie komponentu który będzie tworzył dane testowe
    - wyciągamy go z kontekstu aplikacji (metoda getBean)
5. Walidacja modelu danych 
    - zbędne encje naley usunąć
    - sprawdzenie czy dane są zapisane w sposób poprawny
    - rozszerzenie lub implementacja interfejsu CrudRepository
6. Prezentacja danych na stronie
    - rejestracja kontrolera @Controller
    - rejestracja request handlerów @RequestMapping
7. Strorzenie obiektow DTO (Data Transfer Object)
8. Stworzenie widoków
9. Implementacja metod z adnotacją @RequestMapping
    - wyciągnięcie danych z BD
    - konwersja z Entity -> DTO
    - osadzenie w ModelMap
    - zwrócenie nazwy widoku

# Szczegółowy przebieg projektu

## Omówienie problemu

Tworzymy aplikację, pozwalającą na generowanie quizów z różnych kategorii

## Stworzenie nowego projektu w Intellij'u
- Używamy Spring Boot
- Definiujemy podstawowe zależności, pozostałe można dodać później w pliku pom.xml
- Trafiliśmy na problem z zależnościami w mavenie
- Skonfigurowaliśmy mavena w pliku ~/.m2/settings.xml

## Założenie projektu na gitlabie
- utworzenie repozytorium lokalnego 
- połączenie go z repozytorium zdalnym 
- stworzenie pliku .gitignore
- stworznenie pierwszego commita

## Tworzymy model danych
Początkowy schemat bazy danych
![Model danych](docs/datamodel/datamodel.jpg "Model danych")

## Tworzymy klasy typy Repository
- Rozszerzamy interfejsy CrudRepository, nie robimy tego w sposób "standardowy"
- dokumentacja: https://docs.spring.io/spring-data/cassandra/docs/1.0.2.RELEASE/reference/html/repositories.html

## Dane testowe
Tworzymy dane testowe przy uruchomieniu aplikacji

## Walidacja modelu danych
Podczas walidacji okazuje się, żę początkowo stworzona encja AnswerSet jest zbędna i można ją pominąć w aplikacji

## Obiekty typu DTO

Obiekty typu [DTO](https://en.wikipedia.org/wiki/Data_transfer_object "DTO") to proste obiekty, odcięte od bazy danych
Tworzymy takie obiekty na podstawie wyciągniętych danych z bazy.
Przekazujemy je do ModelMap w kontrolerze.
Kontroler użyje tych obiektów podczas przetwarzania plików jsp.

## Tworzymy widoki
Widoki to pliki jsp które będą przetwarzane przez kontrolery aby wygenerować response w postaci HTML.
Pliki jsp muszą znaleźć się w podkatalogu webapp/WEB-INF/jsp. Ta ścieżka jest skonfigurowana w pliku application.properties. 

## Prezentacja danych na stronie
Tworzymy i rejestrujemy kontroler.

Kontroler ma za zadanie:
- wyciągnąć dane z bazy danych (najlepiej przy użyciu jakiegoś serwisu)
- dokonać konwersji z obiektów typu @Entity na obiekty DTO
- przekazać obiekt DTO do modelu
- zwrócić widok 

# Deployment na Heroku
Generalna instrukcja: https://devcenter.heroku.com/articles/deploying-java

## Etapy pracy

### Uruchomienie lokalne przy uzywając Heroku:
- dodanie drivera postgreSQL do pom.xml
- uruchomienie bazy (patrz nizej) najlepiej z takimi samymi parametrami jak sql, czyli ten sam user, hasło i baza
- dodanie pliku Procfile
- uruchomienie lokalne przy uzyciu Heroku cli komenda: "heroku local web" 
- obserwacja logów w poszukiwaniu portu tomcata

### Uruchomienie w chmurze:
- przeniesienie usera, jego hasła i nazwy bazy do zmiennych uruchomieniowych (stworzenie osobnego run commanda dla mysql'a i postgres'a ). Patrz niżej jak skonfigurować zmienne.
- deployment w chumurze Heroku

### Uruchomienie bazy

#### Standalone
Instalacja i uruchomienie we własnym zakresie, w zalezności od systemu operacyjnego

#### Z użyciem dockera:
- dokumentacja: https://hub.docker.com/_/postgres
- user: postgres
- baza: postgres
- hasło: password
- komenda "docker run --name postgres -e POSTGRES_PASSWORD=password -e POSTGRES_USER=root -e POSTGRES_DB=quizy -p 5432:5432 -d postgres"

#### W chmurze Heroku
- dokumentacja: https://devcenter.heroku.com/categories/heroku-postgres
- po utworzeniu aplikacji w heroku należy zdefiniować zmienną DATABASE_TYPE=postgres
![Zmienna z typem bazy danych](docs/heroku-deployment/heroku-variables.png "Ustawienia projektu dla bazy postgres")  

### Konfiguracja zmiennych uruchomieniowych
Należy skonfigurować następne zmienne uruchomieniowe:
- DATABASE_HOST (IP/nazwa DNS hosta bazy danych)
- DATABASE_HOST (port bazy danych)
- DATABASE_NAME (nazwa bazy)
- DATABASE_USER (użytkownik bazy)
- DATABASE_PASSWORD (hasło do bazy)
- DATABASE_TYPE (wartości: mysql, postgres)
- DATABASE_OPTIONS (zmienna opcjonalna)

![Zmienne dla postgres](docs/local-deployment/postgres-settings.png "Ustawienia projektu dla bazy postgres")
![Zmienne dla mysql](docs/local-deployment/mysql-settings.png "Ustawienia projektu dla bazy mysql")

# Uruchamianie projektu używając mvn

### Ustawienie zmiennych środowiskowych

Zmienne trzeba ustawić przy tworzeniu nowego okna terminala
```shell script
export DATABASE_TYPE=mysql
export DATABASE_HOST=localhost
export DATABASE_PORT=3306
export DATABASE_NAME=quizy
export DATABASE_USERNAME=root
export DATABASE_PASSWORD=password
export DATABASE_OPTIONS="characterEncoding=UTF-8&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC"
```

### Uruchamianie projektu
```shell script
mvn clean compile spring-boot:run
````

### Debugowanie projektu

```shell script
mvn clean compile -Dspring-boot.run.jvmArguments="-Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=5005"
```