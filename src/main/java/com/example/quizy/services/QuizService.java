package com.example.quizy.services;

import org.springframework.stereotype.Component;
import com.example.quizy.converters.QuizConverter;
import com.example.quizy.dto.Quiz;
import com.example.quizy.entities.Category;
import com.example.quizy.entities.Question;
import com.example.quizy.repositories.CategoryRepository;
import com.example.quizy.repositories.QuestionRepository;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;


@Component
public class QuizService {

	@Resource(name = "questionRepository")
	QuestionRepository questionRepository;

	@Resource(name = "categoryRepository")
	CategoryRepository categoryRepository;

	@Resource
	QuizConverter quizConverter;

	/**
	 * I'm able to generate a quiz for given category with a number of question
	 * @param categoryName
	 * @return set of questions
	 */
	public Quiz generateQuiz(String categoryName){
		//Pobieranie danych z bazy
		Category category = categoryRepository.findByName(categoryName);

		//faza 2 pytania przekonwertować np, do obiektu typu Quiz
		Quiz quiz = quizConverter.getQuiz(category);

		//faza 1: pobieramy z bazy wszystkie pytania z danej kategorii
		//dopracować Repository

		return quiz;
	}

	public Quiz checkQuiz(Quiz quiz){
		quiz.getQuestions().stream().forEach(q -> {
			Question question = questionRepository.findById(q.getId()).get();

			Integer correctAnswerId = question.getAnswerSet().getAnswers().stream()
					.filter(a -> a.isCorrect())
					.map(a -> a.getId())
					.findFirst()
					.get();

			q.setAnsweredCorrectly(correctAnswerId.equals(q.getProvidedAnswerId()));
			q.setText(question.getText());
		});

		return quiz;
	}
}
