package com.example.quizy.dto;

import java.util.List;

public class Quiz {

	private String category;
	List<Question> questions;

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public List<Question> getQuestions() {
		return questions;
	}

	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}
}
