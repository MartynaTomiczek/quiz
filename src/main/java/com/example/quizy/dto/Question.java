package com.example.quizy.dto;

import java.util.ArrayList;
import java.util.List;

public class Question {

	private String text;
	private Integer id;
	public List<Answer> answers = new ArrayList<>();

	private Integer providedAnswerId;
	private boolean answeredCorrectly;

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public List<Answer> getAnswers() {
		return answers;
	}

	public Integer getProvidedAnswerId() {
		return providedAnswerId;
	}

	public void setProvidedAnswerId(Integer providedAnswerId) {
		this.providedAnswerId = providedAnswerId;
	}

	public void setAnsweredCorrectly(boolean answeredCorrectly) {
		this.answeredCorrectly = answeredCorrectly;
	}

	public boolean getAnsweredCorrectly() {
		return answeredCorrectly;
	}
}



