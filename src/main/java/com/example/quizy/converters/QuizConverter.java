package com.example.quizy.converters;

import org.springframework.stereotype.Component;
import com.example.quizy.dto.Answer;
import com.example.quizy.dto.Quiz;
import com.example.quizy.entities.Category;
import com.example.quizy.entities.Question;

import java.util.ArrayList;
import java.util.Collection;

@Component
public class QuizConverter {

	public Quiz getQuiz(Category category) {
		Quiz quizDTO = new Quiz();
		quizDTO.setCategory(category.getName());

		quizDTO.setQuestions(new ArrayList<>());

		category.getQuestions().stream().forEach( q->{

			com.example.quizy.dto.Question questionDTO = new com.example.quizy.dto.Question();
			questionDTO.setId(q.getId());
			questionDTO.setText(q.getText());

			q.getAnswerSet().getAnswers().stream().forEach( a -> {
				com.example.quizy.dto.Answer answerDTO = new com.example.quizy.dto.Answer();

				answerDTO.setId(a.getId());
				answerDTO.setText(a.getText());

				questionDTO.getAnswers().add(answerDTO);
			});

			quizDTO.getQuestions().add(questionDTO);
		});

		return quizDTO;
	}
}


