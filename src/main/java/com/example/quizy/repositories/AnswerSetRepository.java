package com.example.quizy.repositories;

import org.springframework.data.repository.CrudRepository;
import com.example.quizy.entities.AnswerSet;
import com.example.quizy.entities.Question;

import javax.annotation.Resource;

@Resource(name = "answerSetRepository")
public interface AnswerSetRepository extends CrudRepository<AnswerSet, Integer> {
}
