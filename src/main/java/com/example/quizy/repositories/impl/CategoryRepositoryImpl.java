package com.example.quizy.repositories.impl;

import com.example.quizy.entities.Category;
import com.example.quizy.repositories.custom.CategoryRepositoryCustom;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

public class CategoryRepositoryImpl implements CategoryRepositoryCustom {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public Category findByName(String categoryName) {
		Query query = entityManager.createQuery("select c from Category as c where c.name = :categoryName");
		query.setParameter("categoryName", categoryName);

		return (Category) query.getSingleResult();
	}
}
