package com.example.quizy.repositories.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import com.example.quizy.entities.Category;
import com.example.quizy.entities.Question;
import com.example.quizy.repositories.custom.QuestionRepositoryCustom;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;
import java.util.Map;

@Component
public class QuestionRepositoryImpl implements QuestionRepositoryCustom {

	@PersistenceContext
	private EntityManager entityManager;

	@Autowired
	JdbcTemplate jdbcTemplate;

	public List<Question> findByCategory(String category){
		Query query = entityManager.createQuery("select q from Question as q where q.category = :categoryName");
		query.setParameter("categoryName", category);

		return query.getResultList();
	}

	public List<Question> findByCategory(Category category){
		Query query = entityManager.createQuery("select q from Question as q where q.category = :category");
		query.setParameter("category", category);

		return query.getResultList();
	}

	public List<Map<String, Object>> findByCategoryUsingSQL(String category){
		String query = "SELECT * FROM QUESTION JOIN CATEGORY ON QUESTION.CATEGORY_ID = CATEGORY.ID WHERE CATEGORY.NAME = ?";
		List<Map<String, Object>> maps = jdbcTemplate.queryForList(query, category);
		return maps;
	}
}
