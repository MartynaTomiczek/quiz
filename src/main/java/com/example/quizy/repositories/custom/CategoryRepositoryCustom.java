package com.example.quizy.repositories.custom;

import com.example.quizy.entities.Category;

public interface CategoryRepositoryCustom {
	Category findByName(String categoryName);
}
