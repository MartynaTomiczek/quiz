package com.example.quizy.repositories.custom;

import com.example.quizy.entities.Category;
import com.example.quizy.entities.Question;

import java.util.List;

public interface QuestionRepositoryCustom {
	List<Question> findByCategory(String category);

	List<Question> findByCategory(Category category);
}
