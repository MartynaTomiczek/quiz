package com.example.quizy.repositories;

import org.springframework.data.repository.CrudRepository;
import com.example.quizy.entities.Category;
import com.example.quizy.repositories.custom.CategoryRepositoryCustom;

import javax.annotation.Resource;

@Resource(name = "categoryRepository")
public interface CategoryRepository extends CrudRepository<Category, Integer>, CategoryRepositoryCustom {
}
