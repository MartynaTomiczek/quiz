package com.example.quizy.repositories;

import org.springframework.data.repository.CrudRepository;
import com.example.quizy.entities.Question;
import com.example.quizy.repositories.custom.QuestionRepositoryCustom;

import javax.annotation.Resource;

@Resource(name = "questionRepository")
public interface QuestionRepository extends CrudRepository<Question, Integer>, QuestionRepositoryCustom {
}
