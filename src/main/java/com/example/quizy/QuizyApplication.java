package com.example.quizy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class QuizyApplication {

	public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(QuizyApplication.class, args);


		TestData testData = context.getBean(TestData.class);

		testData.addQuizJeden();
		testData.addQuizDwa();
		testData.addQuizTrzy();

	}


}
