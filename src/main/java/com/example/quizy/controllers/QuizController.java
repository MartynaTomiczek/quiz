package com.example.quizy.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import com.example.quizy.dto.Quiz;
import com.example.quizy.services.QuizService;

import javax.annotation.Resource;

@Controller
@RequestMapping(value = "/quiz")
public class QuizController {

	@Resource
	QuizService quizService;

	@GetMapping
	public String generateQuiz(@RequestParam String categoryName, ModelMap model){

		Quiz quiz = quizService.generateQuiz(categoryName);

		model.put("quiz", quiz);

		return "quiz";
	}

	@PostMapping
	public String handleQuiz(@ModelAttribute Quiz quiz, ModelMap model){

		quizService.checkQuiz(quiz);

		model.put("quiz", quiz);

		return "summary";
	}
}