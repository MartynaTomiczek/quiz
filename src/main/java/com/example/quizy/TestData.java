package com.example.quizy;

import org.springframework.stereotype.Component;
import com.example.quizy.entities.Answer;
import com.example.quizy.entities.AnswerSet;
import com.example.quizy.entities.Category;
import com.example.quizy.entities.Question;
import com.example.quizy.repositories.CategoryRepository;
import com.example.quizy.repositories.QuestionRepository;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Component
public class TestData {

    @Resource(name = "categoryRepository")
    private CategoryRepository categoryRepository;

    @Resource(name = "questionRepository")
    private QuestionRepository questionRepository;

    public void addQuizJeden() {
        Category category = new Category();
        category.setName("Wiedza ogólna");

        Question question1 = createQuestion(category,
                "Który z płazów jest największy?",
                "żaba",
                "waran",
                "wąż boa",
                "anakonda"
        );

        Question question2 = createQuestion(category,
                "Tuzin ile to sztuk?",
                "12",
                "15",
                "10",
                "8"
        );

        Question question3 = createQuestion(category,
                "Boisko w której dyscyplinie jest najmniejsze?",
                "piłka nożna",
                "golf",
                "tenis",
                "sztafeta"
        );

        List<Question> questionsList = new ArrayList<>();
        questionsList.add(question1);
        questionsList.add(question2);
        questionsList.add(question3);

        questionRepository.saveAll(questionsList);
    }

    public void addQuizDwa() {

        Category category = new Category();
        category.setName("Geografia");

        Question question1 = createQuestion(category, "Stolicą Hiszpanii jest: ",
                "Madryt",
                "Malaga",
                "Barcelona",
                "Sewilla");
        Question question2 = createQuestion(category, "Najgłębsze jezioro w Polsce to: ",
                "Hańcza",
                "Łebsko",
                "Solina",
                "Drawsko");

        Question question3 = createQuestion(category, "Ile stanów jest w Stanach Zjednoczonych",
                "50",
                "49",
                "52",
                "47"
                );

        List<Question> questionList = new ArrayList<>();
        questionList.add(question1);
        questionList.add(question2);
        questionList.add(question3);

        questionRepository.saveAll(questionList);

    }

    public void addQuizTrzy() {
        Category category = new Category();
        category.setName("Historia");

        Question question1 = createQuestion(category, "W którym roku wybuchła I wojna światowa?",
                "1914",
                "1917",
                "1918",
                "1920"
        );

        Question question2 = createQuestion(category, "Pierwsza stolica Polski to:",
                "Gniezno",
                "Poznań",
                "Wrocław",
                "Biskupin"
        );

        List<Question> questionsList = new ArrayList<>();
        questionsList.add(question1);
        questionsList.add(question2);

        questionRepository.saveAll(questionsList);
    }



    private Question createQuestion(Category category, String questionText, String correctAnswerText, String... wrongAnswerTexts) {
        Question question = createQuestion(category, questionText);

        createAnswer(question, correctAnswerText, true);

        for (String wrongAnswerText : wrongAnswerTexts) {
            createAnswer(question, wrongAnswerText, false);
        }

        return question;
    }

    private Question createQuestion(Category category, String questionText) {
        Question question = new Question();
        question.setText(questionText);

        question.setCategory(category);
        question.setAnswerSet(new AnswerSet());
        return question;
    }

    private void createAnswer(Question question, String wrongAnswerText, boolean b) {
        Answer wrongAnswer = new Answer();
        wrongAnswer.setText(wrongAnswerText);
        wrongAnswer.setCorrect(b);
        wrongAnswer.setAnswerSet(question.getAnswerSet());
        question.getAnswerSet().getAnswers().add(wrongAnswer);
    }
}