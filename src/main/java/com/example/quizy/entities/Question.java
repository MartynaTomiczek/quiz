package com.example.quizy.entities;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import javax.persistence.*;

@Entity
public class Question {
	@Id
	@GeneratedValue
	private Integer id;

	private String text;

	@OneToOne
	@Cascade(CascadeType.PERSIST)
	private AnswerSet answerSet;

	@ManyToOne
	@Cascade(CascadeType.PERSIST)
	private Category category;

	public Integer getId() {
		return id;
	}

	public String getText() {
		return this.text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public void setAnswerSet(AnswerSet answerSet) {
		this.answerSet = answerSet;
	}

	public AnswerSet getAnswerSet() {
		return answerSet;
	}
}
