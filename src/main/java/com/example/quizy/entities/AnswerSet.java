package com.example.quizy.entities;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
//Auto
public class AnswerSet {

    @Id
    @GeneratedValue
    Integer id;

    @OneToOne(mappedBy = "answerSet")
    @Cascade(CascadeType.PERSIST)
    private Question question;

    // Jestes w Klasie AnswerSet. Wyobraź sobie, że jest to auto. Auto chce sprawdzić jakie koła do niego należą.
    // Żeby to sprawdzić trzeba dostać się do tabeli z kołami(Answer) poprzez dodatnie tu => mappedBy = "answerSet")
    // W Klasie Anser w Bazie danych będzie FKey answerSet. Poniższe 3 linijki kodu o tym mówią.

    @OneToMany(mappedBy = "answerSet")
     @Cascade(CascadeType.PERSIST)
    // @OneToMany czyli Każdy One AnswerSet(Każde auto) ma jedną odpowiedź(jedno koło)
    private Set<Answer> answers = new HashSet<>();

    public AnswerSet() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public Set<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(Set<Answer> answers) {
        this.answers = answers;
    }
}
