<%--@elvariable id="quiz" type="com.example.quizy.dto.Quiz"--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="forms" uri="http://www.springframework.org/tags/form" %>

<html>
    <body>
        <div>
            Quizy ${quiz.category}!
            <hr/>
        </div>

        <forms:form modelAttribute="quiz" action="/quiz" method="post">
            <div>
                <c:forEach var="question" items="${quiz.questions}" varStatus="questionLoop">
                    <br/>
                    <form:hidden path="questions[${questionLoop.index}].id" />
                    <div>
                        <b><c:out value="${question.text}"/></b>

                        <c:forEach var="answer" items="${question.answers}" varStatus="answerLoop">
                            <div>
                                <form:radiobutton label="${answer.text}" value="${answer.id}" path="questions[${questionLoop.index}].providedAnswerId" />
                            </div>
                        </c:forEach>
                        </div>
                </c:forEach>
            </div>
            <input type="submit" value="Check">
        </forms:form>
    </body>
</html>

