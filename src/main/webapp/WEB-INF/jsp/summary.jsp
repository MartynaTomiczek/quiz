<%--@elvariable id="quiz" type="com.example.quizy.dto.Quiz"--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="forms" uri="http://www.springframework.org/tags/form" %>

<html>
<body>

<div>
    Podsumowanie
    <hr/>
</div>

<c:forEach var="question" items="${quiz.questions}">
    <br/>
    <div style="color: ${question.answeredCorrectly ? 'blue' : 'red'}">
        <b><c:out value="${question.text}"/></b>
    </div>
</c:forEach>

</body>
</html>